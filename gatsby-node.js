const dotenv = require('dotenv');
const path = require('path');

dotenv.config({
  path: '.env',
});

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;
  const ArticleTemplate = path.resolve('src/templates/article-template.jsx');

  return graphql(`
  {
    allContentfulArticle {
      edges {
        node {
          slug
          categories {
            name
          }
          author {
            names
            lastName  
          }
        }
      }
    }
  }
  `).then((result) => {
    if (result.errors) {
      throw result.errors;
    }

    result.data.allContentfulArticle.edges.forEach(({ node }) => {
      createPage({
        // Path for this page — required
        path: `/articulo/${node.slug}`,
        component: ArticleTemplate,
        context: {
          slug: node.slug,
        },
      });
    });
  });
};

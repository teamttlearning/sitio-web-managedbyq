import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'gatsby';
import { Grid, Row, Cell } from '@material/react-layout-grid';
import {
  Headline3, Subtitle1, Body1,
} from '@material/react-typography';
import ReactMarkdown from 'react-markdown';
import Layout from '../components/layout';


const ArticleTemplate = ({
  data: {
    contentfulArticle,
  },
}) => (
  <Layout>
    <Grid>
      <Row>
        <Cell desktopColumns={2} />
        <Cell desktopColumns={2}>
          <Subtitle1>
            {contentfulArticle.categories.name}
          </Subtitle1>
        </Cell>
      </Row>
      <Row>
        <Cell desktopColumns={2} />
        <Cell desktopColumns={8}>
          <Headline3>
            {contentfulArticle.title}
          </Headline3>
        </Cell>
      </Row>
      <img src={`https://${contentfulArticle.image[0].fluid.src}`} alt=" " style={{ marginBottom: '2rem', marginTop: '3rem', marginLeft: '10%' }} width="80%" />
      <Row>
        <Cell desktopColumns={2} />
        <Cell desktopColumns={8}>
          <Body1 className="mdc-typography--body1--article-text">
            <ReactMarkdown
              className="mdc-markdown-text"
              source={contentfulArticle.text.text}
            />
          </Body1>
        </Cell>
      </Row>
    </Grid>
  </Layout>
);

ArticleTemplate.propTypes = {
  data: PropTypes.shape({
    contentfulArticle: PropTypes.object,
  }),
};

export const query = graphql`
  query articleQuery(
    $slug: String!,
  ) {
    contentfulArticle(slug: {
      eq: $slug
    }) {
      author {
        name
        lastName
        rol
      }
      image{
        fluid{
          src
        }
      }
      text{
        text
      }
      categories {
        name
        id
      }
      title
    }
  }
`;

export default ArticleTemplate;

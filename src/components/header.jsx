import PropTypes from 'prop-types';
import { navigate } from 'gatsby'
import React from 'react';

// Material Components
import TopAppBar, {
  TopAppBarFixedAdjust,
  TopAppBarIcon,
  TopAppBarRow,
  TopAppBarSection,
} from '@material/react-top-app-bar';
import MaterialIcon from '@material/react-material-icon';

const Header = ({ siteTitle }) => (
  <div>
    <TopAppBar
      fixed
    >
      <TopAppBarRow>
        <TopAppBarSection
          align="end"
        >
          <TopAppBarIcon
            className="mdc-top-app-bar__menu-icon"
            navIcon
            tabIndex={0}
          >
            <MaterialIcon
              hasRipple
              icon="menu"
              onClick={() => console.log('click')}
            />
          </TopAppBarIcon>
        </TopAppBarSection>

        <TopAppBarSection align="start">
          <TopAppBarIcon
            className="mdc-top-app-bar__menu-icon"
            navIcon
            tabIndex={0}
          >
            <MaterialIcon
              hasRipple
              icon="trip_origin"
              className="logoIcon"
              onClick={() => navigate('/')}
            />
          </TopAppBarIcon>
        </TopAppBarSection>
      </TopAppBarRow>
    </TopAppBar>
    <TopAppBarFixedAdjust />
  </div>
);

Header.propTypes = {
  siteTitle: PropTypes.string,
};

export default Header;

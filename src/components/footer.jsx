import React from 'react';
import { Link } from 'gatsby';


import { Grid, Row, Cell } from '@material/react-layout-grid';
import { Subtitle1 } from '@material/react-typography';

import TextField, { Input } from '@material/react-text-field';
import FooterSolutions from './footer-ops';


const Footer = () => (
  <Grid>
    <Row>
      <Cell desktopColumns={3}>
        <h3>
        The Operating Sistem for Offcie
        </h3>
      </Cell>
    </Row>
    <Row>
      <Cell desktopColumns={1} />
      <Cell desktopColumns={10}>
        <Row />
      </Cell>
    </Row>
  </Grid>
);

Footer.propTypes = {};

export default Footer;

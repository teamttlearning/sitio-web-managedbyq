/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

// Estilos
import '../../styles/sass/layout-grid.scss';
import '../../styles/sass/top-app-bar.scss';
import '../../styles/sass/icons.scss';
import '../../styles/sass/typography.scss';
import '../../styles/sass/card.scss';
import '../../styles/sass/textfield.scss';

import React from 'react';
import PropTypes from 'prop-types';
import { useStaticQuery, graphql } from 'gatsby';

// Componentes
import { Global, css } from '@emotion/core';
import Header from './header';
import Footer from './footer';

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      contentfulProyectData {
        title
      }
    }
  `);

  return (
    <>
      <Global
        styles={css`body { margin: 0; }`}
      />
      <Header siteTitle={data.contentfulProyectData.title} />
      <div>
        {children}
      </div>
      <Footer />
    </>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;

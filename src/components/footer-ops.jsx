import React from 'react';
import PropTypes from 'prop-types';

// Componentes
import { Cell, Row } from '@material/react-layout-grid';
import { Subtitle1 } from '@material/react-typography';

const FooterSolutions = () => (
  <>
    <div>
      <Row>
        <Subtitle1>
          <strong>
          Solution
          </strong>
        </Subtitle1>
      </Row>
      <p>
        <Cell desktopColumns={2}>
        FacitiesService
        </Cell>
      </p>
      <p>
          WorkplaceStaffing
      </p>
      <p>
        TaskManagement
      </p>
    </div>
  </>
);


FooterSolutions.propTypes = {};
export default FooterSolutions;

import React from 'react';
import PropTypes from 'prop-types';

// Componentes
import { Cell } from '@material/react-layout-grid';

// Componentes Personaliados
import { useStaticQuery, graphql } from 'gatsby';
import ArticleCard from './article-card';

const Articles = () => {
  const data = useStaticQuery(graphql`
    query MyQuery {
      allContentfulArticle {
        edges {
          node {
            title
            slug
            abstract
            image {
              fluid {
                src
              }
            }
            author {
              name
              lastName
              rol
            }
            categories {
              name
            }
          }
        }
      }
    }`);
  return (
    <>
      {
        data.allContentfulArticle.edges.map((article) => (
          <Cell desktopColumns={4}>
            <ArticleCard
              image={article.node.image[0].fluid.src}
              title={article.node.title}
              category={article.node.categories.name}
              slug={article.node.slug}
              abstract={article.node.abstract}
            />
          </Cell>
        ))
      }
    </>
  );
};

Articles.propTypes = {};

export default Articles;

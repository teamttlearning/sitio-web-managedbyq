import React from 'react';
import PropTypes from 'prop-types';
import { navigate } from 'gatsby';

// componentes
import { Headline6, Body2, Caption } from '@material/react-typography';
import Card, {
  CardPrimaryContent,
  CardMedia,
  CardActions,
} from '@material/react-card';

const ArticleCard = ({
  image,
  title,
  slug,
  abstract,
  category,
}) => (
  <Card className="mdc-card--outlined">
    <CardPrimaryContent>
      <CardMedia
        className="mdc-card__media--16-9"
        imageUrl={`https://${image}`}
        onClick={
        () => (
          navigate(`/articulo/${slug}`)
        )
      }
      />
    </CardPrimaryContent>
    <CardActions>
      <Caption>
        {category}
      </Caption>
    </CardActions>
    <Headline6 onClick={
        () => (
          navigate(`/articulo/${slug}`)
        )
    }
    >
      {title}
    </Headline6>
    <Body2>
      {abstract}
    </Body2>
  </Card>
);


ArticleCard.propTypes = {
  image: PropTypes.string,
  title: PropTypes.string,
  slug: PropTypes.string,
  category: PropTypes.string,
  abstract: PropTypes.string,
};

export default ArticleCard;

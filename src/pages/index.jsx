import React from 'react';

// Componentes
import { Grid, Row, Cell } from '@material/react-layout-grid';
import { Headline2 } from '@material/react-typography';

// Componentes personalizados
import Layout from '../components/layout';
import Image from '../components/image';
import SEO from '../components/seo';
import Articles from '../components/article/articles';

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <Grid>
      <Row>
        <Cell desktopColumns={5}>
          <Headline2>
            <strong>
              All Hands
            </strong>
          </Headline2>
        </Cell>
      </Row>
      <Row>
        <Articles />
      </Row>
    </Grid>
  </Layout>
);

export default IndexPage;
